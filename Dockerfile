# Build stage
FROM maven:3.6.0-jdk-11-slim AS build
WORKDIR /usr/app
COPY . .
RUN mvn clean package

# Run stage
FROM openjdk:8-jdk-alpine
ARG JAR_FILE
COPY --from=build /usr/app/${JAR_FILE} /app.jar
CMD ["java", "-jar", "/app.jar"]