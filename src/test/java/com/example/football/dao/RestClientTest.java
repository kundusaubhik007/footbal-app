package com.example.football.dao;

import com.example.football.model.Standing;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

import static org.junit.Assert.*;

public class RestClientTest {


    @Test
    public void testGet() throws IOException {
        ResponseEntity<String> stringResponseEntity = new RestClient().get("https://apiv2.apifootball.com/?action=get_standings&league_id=149&APIkey=9bb66184e0c8145384fd2cc0f7b914ada57b4e8fd2e4d6d586adcc27c257a978");
        ObjectMapper objectMapper = new ObjectMapper();
        Standing[] standings = objectMapper.readValue(stringResponseEntity.getBody(), Standing[].class);
        assertNotNull(standings);
    }

}