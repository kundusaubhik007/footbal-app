package com.example.football.service;

import com.example.football.configuration.Configuration;
import com.example.football.dao.RestClient;
import com.example.football.exception.StandingException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class CountryServiceTest {

    @InjectMocks
    private CountryService countryService;

    @Mock
    private RestClient restClient;

    @Mock
    private Configuration configuration;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        when(configuration.getApiKey()).thenReturn("apikey");
        when(configuration.getFootballAPIBaseURL()).thenReturn("baseurl");
    }

    @Test
    public void getCountryID() throws Exception {
        String returnedFromAPI = "[\n" +
                "  {\n" +
                "\t\"country_id\": \"41\",\n" +
                "\t\"country_name\": \"England\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"country_id\": \"68\",\n" +
                "    \"country_name\": \"Italy\"\n" +
                "  }]";
        when(restClient.get(anyString())).thenReturn(new ResponseEntity(returnedFromAPI, HttpStatus.OK));
        String[] countryId = countryService.getCountryIDs("England");
        assertEquals("41", countryId[0]);
    }

    @Test(expected = StandingException.class)
    public void getCountryIDThrowsException() throws Exception {
        when(restClient.get(anyString())).thenReturn(new ResponseEntity("", HttpStatus.BAD_REQUEST));
        countryService.getCountryIDs("Invalid");
    }
}