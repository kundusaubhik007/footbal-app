package com.example.football.service;

import com.example.football.configuration.Configuration;
import com.example.football.dao.RestClient;
import com.example.football.exception.StandingException;
import com.example.football.model.CountryToLeagueID;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class LeagueServiceTest {

    @InjectMocks
    private LeagueService leagueService;

    @Mock
    private RestClient restClient;

    @Mock
    private Configuration configuration;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        when(configuration.getApiKey()).thenReturn("apikey");
        when(configuration.getFootballAPIBaseURL()).thenReturn("baseurl");
    }

    @Test
    public void getLeagueIDTest() throws Exception {
        String returnedFromAPI = "[\n" +
                "  {\n" +
                "    \"country_id\": \"41\",\n" +
                "    \"country_name\": \"England\",\n" +
                "    \"league_id\": \"149\",\n" +
                "    \"league_name\": \"Championship\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"country_id\": \"41\",\n" +
                "    \"country_name\": \"England\",\n" +
                "    \"league_id\": \"8640\",\n" +
                "    \"league_name\": \"EFL Cup\"\n" +
                "  }]";
        when(restClient.get(anyString())).thenReturn(new ResponseEntity(returnedFromAPI, HttpStatus.OK));
        CountryToLeagueID[] countryId = leagueService.getCountryToLeagueIDs("Championship", new String[]{"41"});
        assertEquals("41", countryId[0].getCountryID());
        assertEquals("149", countryId[0].getLeagueID());
    }

    @Test(expected = StandingException.class)
    public void getLeagueIDThrowsException() throws Exception {
        when(restClient.get(anyString())).thenReturn(new ResponseEntity("", HttpStatus.BAD_REQUEST));
        leagueService.getCountryToLeagueIDs("Invalid", new String[]{"99999"});
    }


}