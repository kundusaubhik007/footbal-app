package com.example.football.service;

import com.example.football.configuration.Configuration;
import com.example.football.dao.RestClient;
import com.example.football.domain.StandingRequest;
import com.example.football.domain.StandingSnapshot;
import com.example.football.exception.StandingException;
import com.example.football.model.CountryToLeagueID;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class StandingServiceTest {

    @InjectMocks
    private StandingService standingService;

    @Mock
    private RestClient restClient;

    @Mock
    private LeagueService leagueService;

    @Mock
    private CountryService countryService;

    @Mock
    private Configuration configuration;



    @Before
    public void init()
    {
        MockitoAnnotations.initMocks(this);
        when(configuration.getApiKey()).thenReturn("apikey");
        when(configuration.getFootballAPIBaseURL()).thenReturn("baseurl");
    }



    @Test
    public void getSnapshotsTest() throws Exception {
        when(countryService.getCountryIDs(anyString())).thenReturn(new String[]{"41"});
        when(leagueService.getCountryToLeagueIDs(anyString(),any())).thenReturn(new CountryToLeagueID[]{new CountryToLeagueID("41","148")});
        when(restClient.get(anyString())).thenReturn(new ResponseEntity(returnedFromAPI, HttpStatus.OK));
        StandingSnapshot[] standingSnapshot = standingService.getStandingSnapshot(new StandingRequest("England", "Premier League", "Manchester City"));
        assertEquals(1, standingSnapshot.length);
        System.out.println(standingSnapshot.toString());
    }

    @Test(expected = StandingException.class)
    public void getLeagueIDThrowsException() throws Exception {
        when(countryService.getCountryIDs(anyString())).thenReturn(new String[]{"41"});
        when(leagueService.getCountryToLeagueIDs(anyString(),any())).thenReturn(new CountryToLeagueID[]{new CountryToLeagueID("41","148")});
        when(restClient.get(anyString())).thenReturn(new ResponseEntity("", HttpStatus.BAD_REQUEST));
        standingService.getStandingSnapshot(new StandingRequest("England", "Premier League", "Manchester City"));
    }

    @Test(expected = StandingException.class)
    public void getLeagueIDThrowsExceptionWhenCountryIDNotFound() throws Exception {
        when(countryService.getCountryIDs(anyString())).thenThrow(new StandingException("Country id not found"));
        standingService.getStandingSnapshot(new StandingRequest("England", "Premier League", "Manchester City"));
    }

    @Test(expected = StandingException.class)
    public void getLeagueIDThrowsExceptionWhenLeagueIDNotFound() throws Exception {
        when(countryService.getCountryIDs(anyString())).thenReturn(new String[]{"41"});
        when(leagueService.getCountryToLeagueIDs(anyString(),any())).thenThrow(new StandingException("League id not found"));
        standingService.getStandingSnapshot(new StandingRequest("England", "Premier League", "Manchester City"));
    }


    private String returnedFromAPI = "[\n" +
            "  {\n" +
            "    \"country_name\": \"England\",\n" +
            "    \"league_id\": \"148\",\n" +
            "    \"league_name\": \"Premier League\",\n" +
            "    \"team_id\": \"2626\",\n" +
            "    \"team_name\": \"Manchester City\",\n" +
            "    \"overall_league_position\": \"1\",\n" +
            "    \"overall_league_payed\": \"38\",\n" +
            "    \"overall_league_W\": \"32\",\n" +
            "    \"overall_league_D\": \"2\",\n" +
            "    \"overall_league_L\": \"4\",\n" +
            "    \"overall_league_GF\": \"95\",\n" +
            "    \"overall_league_GA\": \"23\",\n" +
            "    \"overall_league_PTS\": \"98\",\n" +
            "    \"home_league_position\": \"1\",\n" +
            "    \"home_league_payed\": \"19\",\n" +
            "    \"home_league_W\": \"18\",\n" +
            "    \"home_league_D\": \"0\",\n" +
            "    \"home_league_L\": \"1\",\n" +
            "    \"home_league_GF\": \"57\",\n" +
            "    \"home_league_GA\": \"12\",\n" +
            "    \"home_league_PTS\": \"54\",\n" +
            "    \"away_league_position\": \"1\",\n" +
            "    \"away_league_payed\": \"19\",\n" +
            "    \"away_league_W\": \"14\",\n" +
            "    \"away_league_D\": \"2\",\n" +
            "    \"away_league_L\": \"3\",\n" +
            "    \"away_league_GF\": \"38\",\n" +
            "    \"away_league_GA\": \"11\",\n" +
            "    \"away_league_PTS\": \"44\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"country_name\": \"England\",\n" +
            "    \"league_id\": \"148\",\n" +
            "    \"league_name\": \"Premier League\",\n" +
            "    \"team_id\": \"2621\",\n" +
            "    \"team_name\": \"Liverpool\",\n" +
            "    \"overall_league_position\": \"2\",\n" +
            "    \"overall_league_payed\": \"38\",\n" +
            "    \"overall_league_W\": \"30\",\n" +
            "    \"overall_league_D\": \"7\",\n" +
            "    \"overall_league_L\": \"1\",\n" +
            "    \"overall_league_GF\": \"89\",\n" +
            "    \"overall_league_GA\": \"22\",\n" +
            "    \"overall_league_PTS\": \"97\",\n" +
            "    \"home_league_position\": \"2\",\n" +
            "    \"home_league_payed\": \"19\",\n" +
            "    \"home_league_W\": \"17\",\n" +
            "    \"home_league_D\": \"2\",\n" +
            "    \"home_league_L\": \"0\",\n" +
            "    \"home_league_GF\": \"55\",\n" +
            "    \"home_league_GA\": \"10\",\n" +
            "    \"home_league_PTS\": \"53\",\n" +
            "    \"away_league_position\": \"2\",\n" +
            "    \"away_league_payed\": \"19\",\n" +
            "    \"away_league_W\": \"13\",\n" +
            "    \"away_league_D\": \"5\",\n" +
            "    \"away_league_L\": \"1\",\n" +
            "    \"away_league_GF\": \"34\",\n" +
            "    \"away_league_GA\": \"12\",\n" +
            "    \"away_league_PTS\": \"44\"\n" +
            "  }]";
}