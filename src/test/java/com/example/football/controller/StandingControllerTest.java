package com.example.football.controller;

import com.example.football.dao.RestClient;
import com.example.football.domain.StandingSnapshot;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.Equals;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StandingControllerTest {

    @LocalServerPort
    private String port;

    @Test
    public void testAPI() throws IOException {
        RestClient restClient = new RestClient();
        LinkedMultiValueMap<String, String> allRequestParams = new LinkedMultiValueMap<>();
        allRequestParams.put("country_name", Arrays.asList("England"));
        allRequestParams.put("league_name", Arrays.asList("Championship"));
        allRequestParams.put("team_name", Arrays.asList("Nottingham"));
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("http://localhost:"+port+"/api/standings")
                .queryParams(allRequestParams);
        UriComponents uriComponents = builder.build().encode();

        ResponseEntity<String> responseEntity = restClient.get(uriComponents.toUriString());
        assertEquals(200, responseEntity.getStatusCode().value());
        System.out.println(responseEntity.getBody());
        ObjectMapper objectMapper = new ObjectMapper();
        StandingSnapshot[] standingSnapshots = objectMapper.readValue(responseEntity.getBody(), StandingSnapshot[].class);
        assertEquals(1, standingSnapshots.length);
    }


}