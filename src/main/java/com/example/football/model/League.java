package com.example.football.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(value = { "country_id", "country_name", "country_logo", "league_season", "league_logo"  })
public class League {

    @JsonProperty("league_id")
    private String leagueID;

    @JsonProperty("league_name")
    private String leagueName;

    public League() {
    }

    public String getLeagueID() {
        return leagueID;
    }

    public String getLeagueName() {
        return leagueName;
    }

}
