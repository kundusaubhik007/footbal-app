package com.example.football.model;

public class CountryToLeagueID {
    private String countryID;
    private String leagueID;

    public CountryToLeagueID(String countryID, String leagueID) {
        this.countryID = countryID;
        this.leagueID = leagueID;
    }

    public String getCountryID() {
        return countryID;
    }

    public String getLeagueID() {
        return leagueID;
    }
}
