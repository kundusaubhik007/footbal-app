package com.example.football.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(value = { "country_logo" })
public class Country {

    @JsonProperty("country_id")
    private String countryID;

    @JsonProperty("country_name")
    private String countryName;

    public Country() {
    }

    public String getCountryID() {
        return countryID;
    }

    public String getCountryName() {
        return countryName;
    }

}
