package com.example.football.controller;

import com.example.football.domain.ExceptionResponse;
import com.example.football.domain.StandingRequest;
import com.example.football.domain.StandingSnapshot;
import com.example.football.exception.StandingException;
import com.example.football.service.StandingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class StandingController {

    @Autowired
    StandingService standingService;

    @RequestMapping(value="/standings", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity getStandingSnapshots(
            @RequestParam("country_name") String countryName,
            @RequestParam("league_name") String leagueName,
            @RequestParam("team_name") String teamName
    ) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        StandingRequest standingRequest = new StandingRequest(countryName,leagueName, teamName);
        try {
            StandingSnapshot[] standingSnapshot = standingService.getStandingSnapshot(standingRequest);
            return new ResponseEntity(standingSnapshot, headers, HttpStatus.OK);
        } catch (StandingException ex) {
            ex.printStackTrace();
            return new ResponseEntity(new ExceptionResponse(404, ex.getMessage()), headers, HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity(new ExceptionResponse(500, "Internal server error"), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
