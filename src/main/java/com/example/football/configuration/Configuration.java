package com.example.football.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("application")
public class Configuration {

    private String footballApiKey;
    private String footballAPIBaseURL;

    public String getApiKey() {
        return footballApiKey;
    }

    public void setFootballApiKey(String footballApiKey) {
        this.footballApiKey = footballApiKey;
    }

    public void setFootballAPIBaseURL(String footballAPIBaseURL) {
        this.footballAPIBaseURL = footballAPIBaseURL;
    }

    public String getFootballAPIBaseURL() {
        return footballAPIBaseURL;
    }
}
