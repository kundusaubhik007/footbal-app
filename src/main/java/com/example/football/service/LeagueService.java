package com.example.football.service;

import com.example.football.configuration.Configuration;
import com.example.football.dao.RestClient;
import com.example.football.exception.StandingException;
import com.example.football.model.Country;
import com.example.football.model.CountryToLeagueID;
import com.example.football.model.League;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class LeagueService {

    Logger logger = LoggerFactory.getLogger(LeagueService.class);
    ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    RestClient restClient;
    @Autowired
    Configuration configuration;

    public CountryToLeagueID[] getCountryToLeagueIDs(String leagueName, String[] countryIDs) throws Exception {
        return Arrays.stream(countryIDs).map(countryID -> {
                    String getLeagueIdUrl = configuration.getFootballAPIBaseURL() + "/?action=get_leagues&country_id=" + countryID + "&APIkey=" + configuration.getApiKey();
                    ResponseEntity<String> leagueResponse = restClient.get(getLeagueIdUrl);
                    if (leagueResponse.getStatusCode().value() != 200) {
                        throw new StandingException("Unable to fetch leagueId from leagueName" + leagueName);
                    }
                    try {
                        League[] leagues = objectMapper.readValue(leagueResponse.getBody(), League[].class);
                        return Arrays.stream(leagues)
                                .filter(league -> league.getLeagueName().equalsIgnoreCase(leagueName))
                                .map(league -> new CountryToLeagueID(countryID, league.getLeagueID()))
                                .toArray(CountryToLeagueID[]::new);
                    } catch (Exception ex) {
                        logger.error("Unable to fetch leagueId from leagueName: " + leagueName, ex.getMessage());
                        throw new StandingException("Unable to fetch leagueId from leagueName: " + leagueName);
                    }
                }
        )
                .flatMap(leagueIDs -> Arrays.stream(leagueIDs))
                .toArray(CountryToLeagueID[]::new);
    }
}
