package com.example.football.service;

import com.example.football.configuration.Configuration;
import com.example.football.dao.RestClient;
import com.example.football.exception.StandingException;
import com.example.football.model.Country;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class CountryService {

    Logger logger = LoggerFactory.getLogger(CountryService.class);
    ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    RestClient restClient;
    @Autowired
    Configuration configuration;

    public String[] getCountryIDs(String countryName) throws Exception {
        String getCountryIdUrl = configuration.getFootballAPIBaseURL()+"?action=get_countries&APIkey="+configuration.getApiKey();
        ResponseEntity<String> countryResponse = restClient.get(getCountryIdUrl);
        if(countryResponse.getStatusCode().value() != 200) {
            throw new StandingException("Unable to fetch countryID from countryName: "+countryName);
        }
        try{
            Country[] countries = objectMapper.readValue(countryResponse.getBody(), Country[].class);
            return Arrays.stream(countries)
                    .filter(country -> country.getCountryName().equalsIgnoreCase(countryName))
                    .map(country -> country.getCountryID()).toArray(String[]::new);
        } catch (Exception ex) {
            logger.error("Unable to fetch countryID from countryName: "+countryName, ex.getMessage());
            throw new StandingException("Unable to fetch countryID from countryName: "+countryName);
        }
    }
}
