package com.example.football.service;

import com.example.football.configuration.Configuration;
import com.example.football.dao.RestClient;
import com.example.football.domain.StandingRequest;
import com.example.football.domain.StandingSnapshot;
import com.example.football.exception.StandingException;
import com.example.football.model.CountryToLeagueID;
import com.example.football.model.Standing;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class StandingService {

    Logger logger = LoggerFactory.getLogger(StandingService.class);

    ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    RestClient restClient;

    @Autowired
    CountryService countryService;

    @Autowired
    LeagueService leagueService;

    @Autowired
    Configuration configuration;


    public StandingSnapshot[] getStandingSnapshot(StandingRequest standingRequest) throws Exception {
        String[] countryIDs = countryService.getCountryIDs(standingRequest.getCountryName());
        logger.info("fetched countryIDs" + Arrays.toString(countryIDs));

        CountryToLeagueID[] countryToLeagueIDS = leagueService.getCountryToLeagueIDs(standingRequest.getLeagueName(), countryIDs);
        logger.info("fetched leagueIDs" + Arrays.toString(Arrays.stream(countryToLeagueIDS).map(countryToLeagueID -> countryToLeagueID.getLeagueID()).toArray(String[]::new)));

        return Arrays.stream(countryToLeagueIDS).map(countryToLeagueID -> {
            String getStandingUrl = configuration.getFootballAPIBaseURL() + "?action=get_standings&league_id=" + countryToLeagueID.getLeagueID() + "&APIkey=" + configuration.getApiKey();
            ResponseEntity<String> standingResponse = restClient.get(getStandingUrl);
            if (standingResponse.getStatusCode().value() != 200) {
                throw new StandingException("Unable to fetch standings from leagueIDs: " + countryToLeagueID.getLeagueID());
            }
            try {
                Standing[] standings = objectMapper.readValue(standingResponse.getBody(), Standing[].class);
                return getStandingSnapshots(countryToLeagueID.getCountryID(), standingRequest.getTeamName(), standings);
            } catch (Exception ex) {
                throw new StandingException("Unable to fetch standings from leagueIDs: " + countryToLeagueID.getLeagueID());
            }
        })
                .flatMap(standingSnapshots -> Arrays.stream(standingSnapshots))
                .toArray(StandingSnapshot[]::new);
    }

    private StandingSnapshot[] getStandingSnapshots(String countryID, String teamName, Standing[] standings) {
        return Arrays.stream(standings).
                filter(standing -> standing.getTeamName().equals(teamName)).
                map(standing -> {
                    return new StandingSnapshot(countryID, standing.getCountryName(),
                            standing.getLeagueId(), standing.getLeagueName(),
                            standing.getTeamId(), standing.getTeamName(), standing.getOverallLeaguePosition());
                }).toArray(StandingSnapshot[]::new);
    }
}
