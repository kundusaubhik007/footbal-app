package com.example.football.exception;

public class StandingException extends RuntimeException{
    private String message;
    public String getMessage() {
        return message;
    }
    public StandingException(String message) {
        this.message = message;
    }
}
