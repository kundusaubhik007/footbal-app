package com.example.football.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestClient {

    Logger logger = LoggerFactory.getLogger(RestClient.class);

    public ResponseEntity<String> get(String serverUrl)
    {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.set("Content-Type", "application/json");
        HttpEntity<?> entity = new HttpEntity<>(requestHeaders); // requestHeaders is of HttpHeaders type
        RestTemplate restTemplate = new RestTemplate();
        logger.debug("Restclient:get:requesturl: "+serverUrl);
        ResponseEntity<String> responseEntity = restTemplate.exchange(serverUrl, HttpMethod.GET,
                entity, String.class);
        return responseEntity;
    }
}
