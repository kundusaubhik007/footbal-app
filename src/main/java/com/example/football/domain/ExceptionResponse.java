package com.example.football.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ExceptionResponse {

    @JsonProperty("code")
    private int code;

    @JsonProperty("error")
    private String error;


    public ExceptionResponse(int code, String error) {
        this.code = code;
        this.error = error;
    }

    @Override
    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        String json = "{}";
        try {
            json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }
}
