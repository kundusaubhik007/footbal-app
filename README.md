# Footbal App

## Run test
```$xslt
mvn clean test
```

## Build docker image and run
```$xslt
docker build --build-arg=/target/football-0.0.1-SNAPSHOT.jar -t dev:latest
docker run -p 8090:8090 dev:latest
```

